# cterasdk-gui

unofficial wxPython implementation with cterasdk to allow for server initialization, portal configuration, and file uploads.



## ctera management agent

cma.py is used for deployment of new portal servers.
- Log file will be generated at cma.log



## ctera upgrade utility
portal_upgrade_tool.py is used to upgrade the CTERA portal software in the appropriate order.
- Log file will be generated at cut.log

