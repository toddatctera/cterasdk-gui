# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Aug  7 2020)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class frameMain
###########################################################################

class frameMain ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"CTERA Management Agent", pos = wx.DefaultPosition, size = wx.Size( 802,718 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		inputSizer = wx.FlexGridSizer( 20, 3, 5, 60 )
		inputSizer.SetFlexibleDirection( wx.BOTH )
		inputSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.targetServerLabel = wx.StaticText( self, wx.ID_ANY, u"Target Server:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.targetServerLabel.Wrap( -1 )

		inputSizer.Add( self.targetServerLabel, 0, wx.ALL, 5 )

		self.address = wx.TextCtrl( self, wx.ID_ANY, u"192.168.1.142", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.address, 0, wx.ALL, 5 )

		self.user = wx.TextCtrl( self, wx.ID_ANY, u"cteraadmin", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.user, 0, wx.ALL, 5 )

		self.password = wx.TextCtrl( self, wx.ID_ANY, u"password", wx.DefaultPosition, wx.Size( 200,-1 ), wx.TE_PASSWORD )
		inputSizer.Add( self.password, 0, wx.ALL, 5 )

		self.ssh_user = wx.TextCtrl( self, wx.ID_ANY, u"root", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.ssh_user, 0, wx.ALL, 5 )

		self.ssh_pass = wx.TextCtrl( self, wx.ID_ANY, u"ctera321", wx.DefaultPosition, wx.Size( 200,-1 ), wx.TE_PASSWORD )
		inputSizer.Add( self.ssh_pass, 0, wx.ALL, 5 )

		self.initDBLabel = wx.StaticText( self, wx.ID_ANY, u"Initalize mainDB:", wx.Point( 250,1 ), wx.DefaultSize, 0 )
		self.initDBLabel.Wrap( -1 )

		inputSizer.Add( self.initDBLabel, 0, wx.ALL, 5 )

		self.dns = wx.TextCtrl( self, wx.ID_ANY, u"DNS Suffix", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.dns, 0, wx.ALL, 5 )

		self.email = wx.TextCtrl( self, wx.ID_ANY, u"Email", wx.Point( -1,-1 ), wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.email, 0, wx.ALL, 5 )

		self.first = wx.TextCtrl( self, wx.ID_ANY, u"First Name", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.first, 0, wx.ALL, 5 )

		self.last = wx.TextCtrl( self, wx.ID_ANY, u"Last Name", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.last, 0, wx.ALL, 5 )

		self.blankLabel1 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.blankLabel1.Wrap( -1 )

		inputSizer.Add( self.blankLabel1, 0, wx.ALL, 5 )

		self.disksLabel = wx.StaticText( self, wx.ID_ANY, u"Disk Selection (DBArchive)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.disksLabel.Wrap( -1 )

		inputSizer.Add( self.disksLabel, 0, wx.ALL, 5 )

		disksChoices = []
		self.disks = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 200,-1 ), disksChoices, 0 )
		self.disks.SetSelection( 0 )
		inputSizer.Add( self.disks, 0, wx.ALL, 5 )

		self.blankLabel2 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.blankLabel2.Wrap( -1 )

		inputSizer.Add( self.blankLabel2, 0, wx.ALL, 5 )

		self.initOthersLabel = wx.StaticText( self, wx.ID_ANY, u"Initalize App/Replica:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.initOthersLabel.Wrap( -1 )

		inputSizer.Add( self.initOthersLabel, 0, wx.ALL, 5 )

		self.main_ip = wx.TextCtrl( self, wx.ID_ANY, u"main_db_ip", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.main_ip, 0, wx.ALL, 5 )

		self.main_name = wx.TextCtrl( self, wx.ID_ANY, u"main_db_name", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.main_name, 0, wx.ALL, 5 )


		bSizer3.Add( inputSizer, 1, wx.ALL|wx.EXPAND, 5 )

		fgSizer2 = wx.FlexGridSizer( 4, 2, 5, 60 )
		fgSizer2.SetFlexibleDirection( wx.BOTH )
		fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.uploadFileLabel = wx.StaticText( self, wx.ID_ANY, u"Upload File:", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		self.uploadFileLabel.Wrap( -1 )

		fgSizer2.Add( self.uploadFileLabel, 0, wx.ALL, 5 )

		self.fileUploadPicker = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.Size( 475,-1 ), wx.FLP_DEFAULT_STYLE )
		fgSizer2.Add( self.fileUploadPicker, 0, wx.ALL, 5 )

		self.uploadPathLabel = wx.StaticText( self, wx.ID_ANY, u"Target Upload Path:", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		self.uploadPathLabel.Wrap( -1 )

		fgSizer2.Add( self.uploadPathLabel, 0, wx.ALL, 5 )

		self.uploadPath = wx.TextCtrl( self, wx.ID_ANY, u"/usr/local/lib/ctera/", wx.DefaultPosition, wx.Size( 475,-1 ), 0 )
		fgSizer2.Add( self.uploadPath, 0, wx.ALL, 5 )

		self.progressLabel = wx.StaticText( self, wx.ID_ANY, u"Progress: ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.progressLabel.Wrap( -1 )

		fgSizer2.Add( self.progressLabel, 0, wx.ALL, 5 )

		self.gauge1 = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( 200,-1 ), wx.GA_HORIZONTAL )
		self.gauge1.SetValue( 0 )
		fgSizer2.Add( self.gauge1, 0, wx.ALL, 5 )


		bSizer3.Add( fgSizer2, 1, wx.EXPAND, 5 )

		commandSizer = wx.FlexGridSizer( 0, 3, 0, 0 )
		commandSizer.SetFlexibleDirection( wx.BOTH )
		commandSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.actionLabel = wx.StaticText( self, wx.ID_ANY, u"Actions: ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.actionLabel.Wrap( -1 )

		commandSizer.Add( self.actionLabel, 0, wx.ALL, 5 )

		actionChoices = [ u"login_test", u"initialize_db", u"initialize_app", u"initialize_replica", u"config_portal", u"initialize_edge", u"upload_file_unix" ]
		self.action = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 200,-1 ), actionChoices, 0 )
		self.action.SetSelection( 0 )
		commandSizer.Add( self.action, 0, wx.ALL, 5 )

		self.loginButton = wx.Button( self, wx.ID_ANY, u"RUN", wx.DefaultPosition, wx.DefaultSize, 0 )
		commandSizer.Add( self.loginButton, 0, wx.ALL, 5 )


		bSizer3.Add( commandSizer, 1, wx.ALL|wx.EXPAND, 15 )


		self.SetSizer( bSizer3 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.loginButton.Bind( wx.EVT_BUTTON, self.runScript )

	def __del__( self ):
		pass


	def runScript( self, event ):
		if(self.action.GetStringSelection() == "login_test"):
			#login_test(self)
			loginTestThread = threading.Thread(target=login_test,args=(self,))
			loginTestThread.start()
			
		if(self.action.GetStringSelection() == "initialize_db"):
			#init_db(self)
			initDBThread = threading.Thread(target=init_db,args=(self,))
			initDBThread.start()

		if(self.action.GetStringSelection() == "initialize_app"):
			#init_app(self)
			initAppThread = threading.Thread(target=init_app,args=(self,))
			initAppThread.start()

		if(self.action.GetStringSelection() == "initialize_replica"):
			#init_replica(self)
			initReplicaThread = threading.Thread(target=init_replica,args=(self,))
			initReplicaThread.start()

		if(self.action.GetStringSelection() == "config_portal"):			
			#config_portal(self)
			configPortalThread = threading.Thread(target=config_portal,args=(self,))
			configPortalThread.start()
		
		if(self.action.GetStringSelection() == "upload_file_unix"):
			uploadThread = threading.Thread(target=upload_file_unix,args=(self,))
			uploadThread.start()
		event.Skip()
		

def login_test(self):
	config.logging.info('Login test started on ' + self.address.Value)
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(self.address.Value, username=self.ssh_user.Value, password=self.ssh_pass.Value)

	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('grep "CTERA_INIT_SERVER_REQUIRED=yes" /etc/ctera/portal.cfg') #Check if server is initalized
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line.rstrip('\n'))
		if not(line.rstrip('\n') == "CTERA_INIT_SERVER_REQUIRED=yes"):
			admin = GlobalAdmin(self.address.Value)
			admin.login(self.user.Value,self.password.Value)
			admin.whoami()

	disk_command = 'grep -e "[sh]d[a-l][0-9]\?" /proc/partitions | awk \'{print $4,expr $3/1024000}\''
	#print(disk_command)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(disk_command)
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
	#	print(line.rstrip('\n'))
		self.disks.Append(line.rstrip('\n')+'GB')
	config.logging.info('Login test complete on ' + self.address.Value)
	return

def init_db(self):
	config.logging.info('Initialize main database server started on ' + self.address.Value)
	admin = GlobalAdmin(self.address.Value)
	admin.setup.init_master(self.user.Value, self.email.Value, self.first.Value, self.last.Value, self.password.Value, self.dns.Value)
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(self.address.Value, username=self.ssh_user.Value, password=self.ssh_pass.Value)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("whoami")
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)

	sperator = ' '
	disk_name=self.disks.GetStringSelection().split(sperator, 1)[0]
	#print(disk_name)
	disk_command='/usr/local/ctera/bin/ctera-storage-util.sh create_db_archive_pool ' + disk_name
	#print(disk_command)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(disk_command)
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)


	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("/usr/local/ctera/bin/ctera.sh configure-db-recovery 7")
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)

	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("/usr/local/ctera/bin/ctera-portal-manage.sh restart")
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)
	config.logging.info('Initialize main database complete on ' + self.address.Value)
	return

def init_app(self):
	config.logging.info('Initialize application server started on ' + self.address.Value)
	admin = GlobalAdmin(self.address.Value)
	admin.setup.init_application_server(self.main_ip.Value, self.ssh_pass.Value)
	memory = "2048"
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(self.address.Value, username=self.ssh_user.Value, password=self.ssh_pass.Value)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("/bin/expr $(free -m |grep -oP '\d+' |head -n 1) / 2")
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		memory = str(line.rstrip('\n'))
		config.logging.info('JAVA_HEAP_LIMIT set to: %r', memory)

	for line in ssh_stderr:
		print(line)

	sed_command = 'sed -i \"s/JAVA_HEAP_LIMIT=.*/JAVA_HEAP_LIMIT={set_memory}/g\" /etc/ctera/portal.cfg'.format(set_memory=str(memory))
	config.logging.info("Executing: " + sed_command)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(sed_command)
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		print(line)
	for line in ssh_stderr:
		print(line)

	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("/usr/local/ctera/bin/ctera-portal-manage.sh restart")
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)
	config.logging.info('Initialize application server complete on ' + self.address.Value)
	return	

def init_replica(self):
	config.logging.info('Initialize replica server started on ' + self.address.Value)
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(self.address.Value, username=self.ssh_user.Value, password=self.ssh_pass.Value)
	sperator = ' '
	disk_name=self.disks.GetStringSelection().split(sperator, 1)[0]
	config.logging.info('Creating DB Archive Pool on ' + disk_name)
	disk_command='/usr/local/ctera/bin/ctera-storage-util.sh create_db_archive_pool ' + disk_name
	#print(disk_command)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(disk_command)
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)

	config.logging.info('Configuring DB Archival')
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("/usr/local/ctera/bin/ctera.sh configure-db-recovery 7")
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)
	
	admin = GlobalAdmin(self.address.Value)
	admin.setup.init_replication_server(self.main_ip.Value, self.ssh_pass.Value, self.main_name.Value)
	config.logging.info('Initialize replica server complete on ' + self.address.Value)
	return


def config_portal(self):
	config.logging.info('Config portal starting on ' + self.address.Value)
	admin = GlobalAdmin(self.address.Value)
	admin.login(self.user.Value,self.password.Value)
	admin.whoami()
	config.logging.info('Config portal complete on ' + self.address.Value)
	return

def upload_file_unix(self):
	config.logging.info('Uploading file to ' + self.address.Value)
	file_name = PurePath(self.fileUploadPicker.GetPath()).name
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(self.address.Value, username=self.ssh_user.Value, password=self.ssh_pass.Value)
	sftp = ssh.open_sftp()
	sftp.chdir(self.uploadPath.Value)
	#config.logger.info(sftp.listdir())
	sftp.put(self.fileUploadPicker.GetPath(), file_name, callback=printTransferProgress)
	sftp.close()
	ssh.close()
	config.logging.info('Upload complete to ' + self.address.Value)
	return

def printTransferProgress(transferred, toBeTransferred):
	frame.gauge1.SetRange(toBeTransferred)
	frame.gauge1.SetValue(transferred)
	if (transferred % 20000 == 0):
		transferred = round(transferred / 1000000, 2)
		toBeTransferred = round(toBeTransferred / 1000000, 2)
		config.logging.info("Transferred: {0}MB\tOut of: {1}MB".format(transferred, toBeTransferred))
		

import os
os.environ['CTERASDK_LOG_FILE'] = 'cma.log'   #Log to local file
from cterasdk import *
from datetime import datetime
from pathlib import PurePath
import paramiko
import sys
from sys import platform
import subprocess
import threading

config.logging.info('Agent Started')
config.http['ssl'] = 'Trust'

#If running Windows, open a PS windows and tail our log
if (platform == "win32"):
	subprocess.Popen('powershell.exe Get-Content ./cma.log –Wait')

#print(os.environ.get('CTERASDK_LOG_FILE'))
#Main Program
app = wx.App()
frame = frameMain(None)
frame.Show()
app.MainLoop()









